<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pl">
<context>
    <name>CustomDialog</name>
    <message>
        <location filename="../dialog.cpp" line="15"/>
        <source>Live environment detected. Please select the root partition of the
 system you want to modify (only Linux partitions are displayed)</source>
        <translation>Wykryto środowisko Live (na żywo). Wybierz partycję root
 systemu, który chcesz zmodyfikować (wyświetlane są tylko partycje Linux)</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="22"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="23"/>
        <source>Cancel</source>
        <translation>Anuluj</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../mainwindow.ui" line="32"/>
        <source>Options</source>
        <translation>Opcje</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="38"/>
        <source>Kernel parameters</source>
        <translation>Parametry jądra (kernel)</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="58"/>
        <source>Set to &apos;0&apos; to boot immediately without displaying the menu, or to &apos;-1&apos; to wait indefinitely</source>
        <translation>Ustaw na &apos;0&apos;, aby uruchomić natychmiast bez wyświetlania menu, lub na &apos;-1&apos;, aby czekać bez określonego czasu</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="61"/>
        <source>Menu timeout</source>
        <translation>Limit czasu menu</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="74"/>
        <source>Boot to</source>
        <translation>Uruchom do</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="81"/>
        <source>Use simplified menu structure without submenus</source>
        <translation>Użyj uproszczonej struktury menu bez podmenu</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="84"/>
        <source>Use flat menus (no submenus)</source>
        <translation>Użyj płaskich menu (bez podmenu)</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="113"/>
        <source>seconds</source>
        <translation>sekund/y</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="120"/>
        <source>With this option enabled whatever entry you select from the grub boot menu will be saved as the new default for future boots</source>
        <translation>Po włączeniu tej opcji, każdy wpis wybrany z menu uruchamiania GRUB zostanie zapisany jako nowa wartość domyślna dla przyszłych uruchomień</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="123"/>
        <source>Enable saving last boot choice</source>
        <translation>Włącz zapisywanie wyboru ostatniego uruchomienia</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="136"/>
        <source>Background</source>
        <translation>Tło</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="154"/>
        <source>Image</source>
        <translation>Obraz</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="167"/>
        <location filename="../mainwindow.ui" line="269"/>
        <source>Enable theme</source>
        <translation>Włącz motyw</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="247"/>
        <source>Splash</source>
        <translation>Ekran powitalny</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="282"/>
        <source>Preview</source>
        <translation>Podgląd</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="305"/>
        <source>Messages</source>
        <translation>Komunikaty</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="311"/>
        <source>Very detailed</source>
        <translation>Bardzo szczegółowe</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="318"/>
        <source>Detailed (default setting)</source>
        <translation>Szczegółowe (ustawienie domyślne)</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="328"/>
        <source>Limited</source>
        <translation>Ograniczone</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="344"/>
        <source>Display log</source>
        <translation>Wyświetl dziennik</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="397"/>
        <source>Display help </source>
        <translation>Wyświetl pomoc</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="400"/>
        <source>Help</source>
        <translation>Pomoc</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="407"/>
        <source>Alt+H</source>
        <translation>Alt+H</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="439"/>
        <source>About this application</source>
        <translation>O programie</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="442"/>
        <source>About...</source>
        <translation>O...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="449"/>
        <source>Alt+B</source>
        <translation>Alt+B</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="484"/>
        <source>Quit application</source>
        <translation>Zakończ aplikację</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="487"/>
        <location filename="../mainwindow.cpp" line="684"/>
        <source>Close</source>
        <translation>Zamknij</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="494"/>
        <source>Alt+N</source>
        <translation>Alt+N</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="535"/>
        <source>Apply</source>
        <translation>Zastosuj</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="74"/>
        <source>Still running</source>
        <translation>Ciągle uruchomiony</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="168"/>
        <source>Installing bootsplash, please wait</source>
        <translation>Instalowanie ekranu powitalnego, proszę czekać</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="175"/>
        <source>Updating sources</source>
        <translation>Aktualizowanie źródeł</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="177"/>
        <source>Installing</source>
        <translation>Instalowanie</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="181"/>
        <source>Error</source>
        <translation>Błąd</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="181"/>
        <source>Could not install the bootsplash.</source>
        <translation>Nie można zainstalować ekranu powitalnego.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="283"/>
        <location filename="../mainwindow.cpp" line="329"/>
        <source>Cannot continue</source>
        <translation>Nie można kontynuować</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="283"/>
        <source>Nothing was selected, cannot change boot options. Exiting...</source>
        <translation>Nic nie zostało wybrane, nie można zmienić opcji uruchamiania. Wychodzenie...</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="329"/>
        <source>Cannot create chroot environment, cannot change boot options. Exiting...</source>
        <translation>Nie można utworzyć środowiska chroot, nie można zmienić opcji uruchamiania. Wychodzenie...</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="576"/>
        <location filename="../mainwindow.cpp" line="887"/>
        <source>Updating configuration, please wait</source>
        <translation>Aktualizowanie konfiguracji, proszę czekać</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="627"/>
        <source>Updating initramfs...</source>
        <translation>Aktualizowanie initramfs...</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="636"/>
        <location filename="../mainwindow.cpp" line="900"/>
        <source>Updating grub...</source>
        <translation>Aktualizowanie GRUB...</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="639"/>
        <source>Done</source>
        <translation>Gotowe</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="639"/>
        <source>Changes have been successfully applied.</source>
        <translation>Zmiany zostały pomyślnie zastosowane.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="652"/>
        <source>About</source>
        <translation>O programie</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="653"/>
        <source>Version: </source>
        <translation>Wersja:</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="654"/>
        <source>Program for selecting common start-up choices</source>
        <translation>Program do zaznaczania typowych wyborów uruchamiania</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="656"/>
        <source>Copyright (c) Garuda Linux</source>
        <translation>Prawa autorskie © Garuda Linux</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="657"/>
        <location filename="../mainwindow.cpp" line="667"/>
        <source>License</source>
        <translation>Licencja</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="658"/>
        <location filename="../mainwindow.cpp" line="673"/>
        <source>Changelog</source>
        <translation>Dziennik zmian</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="659"/>
        <source>Cancel</source>
        <translation>Anuluj</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="703"/>
        <source>Garuda Boot Options</source>
        <translation>Garuda Opcje uruchamiania</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="715"/>
        <source>Running in a Virtual Machine</source>
        <translation>Uruchomienie w maszynie wirtualnej</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="716"/>
        <source>You current system is running in a Virtual Machine,
Plymouth bootsplash will work in a limited way, you also won&apos;t be able to preview the theme</source>
        <translation>Twój obecny system działa na maszynie wirtualnej,
ekran powitalny Plymouth będzie działał w ograniczonym zakresie, nie będzie też można wyświetlić podglądu motywu</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="721"/>
        <source>Plymouth packages not installed</source>
        <translation>Pakiety Plymouth nie są zainstalowane</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="721"/>
        <source>Plymouth packages are not currently installed.
OK to go ahead and install them?</source>
        <translation>Pakiety Plymouth nie są obecnie zainstalowane.
Czy kontynuować i zainstalować je?</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="742"/>
        <source>Images (*.png *.jpg *.jpeg *.tga)</source>
        <translation>Obrazy (*.png *.jpg *.jpeg *.tga)</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="850"/>
        <source>Press any key to close</source>
        <translation>Naciśnij dowolny klawisz, aby zamknąć</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="852"/>
        <source>Log not found</source>
        <translation>Dziennik nie znaleziony</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="852"/>
        <source>Could not find log at </source>
        <translation>Nie można znaleźć dziennika w</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="866"/>
        <source>Needs reboot</source>
        <translation>Konieczne ponowne uruchomienie</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="866"/>
        <source>Plymouth was just installed, you might need to reboot before being able to display previews</source>
        <translation>Plymouth został właśnie zainstalowany, może być konieczne ponowne uruchomienie komputera, zanim będzie można wyświetlić podgląd</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="922"/>
        <source>Click to select theme</source>
        <translation>Kliknij, aby wybrać motyw</translation>
    </message>
</context>
<context>
    <name>QApplication</name>
    <message>
        <location filename="../main.cpp" line="58"/>
        <source>You must run this program as root.</source>
        <translation>Musisz uruchomić ten program jako administrator. </translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../mainwindow.cpp" line="741"/>
        <source>Select image to display in bootloader</source>
        <translation>Wybierz obraz do wyświetlenia w programie rozruchowym</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="931"/>
        <source>Select GRUB theme</source>
        <translation>Wybierz motyw GRUB</translation>
    </message>
</context>
</TS>

# **********************************************************************
# * Copyright (C) 2017 Authors
# *
# * Authors: Adrian, Dolphin Oracle
# *          MX Linux http://mxlinux.org
# *
# * This is free software: you can redistribute it and/or modify
# * it under the terms of the GNU General Public License as published by
# * the Free Software Foundation, either version 3 of the License, or
# * (at your option) any later version.
# *
# * This program is distributed in the hope that it will be useful,
# * but WITHOUT ANY WARRANTY; without even the implied warranty of
# * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# * GNU General Public License for more details.
# *
# * You should have received a copy of the GNU General Public License
# * along with this package. If not, see <http://www.gnu.org/licenses/>.
# **********************************************************************/

QT       += core gui
CONFIG   += c++11

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = garuda-boot-options
TEMPLATE = app

# Build section
BUILD_PREFIX = $$(CA_BUILD_DIR)

isEmpty( BUILD_PREFIX ) {
        BUILD_PREFIX = ./build
}

MOC_DIR       = $$BUILD_PREFIX/moc-qt5
OBJECTS_DIR   = $$BUILD_PREFIX/obj-qt5
RCC_DIR	      = $$BUILD_PREFIX/qrc-qt5
UI_DIR        = $$BUILD_PREFIX/uic-qt5

# Disable QDebug on Release build
#CONFIG(release, debug|release):DEFINES += QT_NO_DEBUG_OUTPUT

# Unused not working
#CONFIG(release):DEFINES += QT_NO_DEBUG_OUTPUT

unix {
        isEmpty(PREFIX) {
                PREFIX = /usr
        }
        BINDIR = $$PREFIX/bin

        target.path = $$BINDIR

       help.path=$$PREFIX//share/doc/garuda-boot-options/help/
       help.files=help/garuda-boot-options.html

        icons.path = $$PREFIX/share/icons/hicolor/scalable/apps/
        icons.files = garuda-boot-options.png

        desktop.path = $$PREFIX/share/applications/
        desktop.files = "garuda-boot-options.desktop"

        INSTALLS += target  desktop help icons
}

SOURCES += main.cpp\
    mainwindow.cpp \
    dialog.cpp \
    mprocess.cpp

HEADERS  += \
    mainwindow.h \
    dialog.h \
    version.h \
    mprocess.h

FORMS    += \
    mainwindow.ui

TRANSLATIONS += translations/garuda-boot-options_am.ts \
                translations/garuda-boot-options_ar.ts \
                translations/garuda-boot-options_bg.ts \
                translations/garuda-boot-options_ca.ts \
                translations/garuda-boot-options_cs.ts \
                translations/garuda-boot-options_da.ts \
                translations/garuda-boot-options_de.ts \
                translations/garuda-boot-options_el.ts \
                translations/garuda-boot-options_es.ts \
                translations/garuda-boot-options_et.ts \
                translations/garuda-boot-options_eu.ts \
                translations/garuda-boot-options_fa.ts \
                translations/garuda-boot-options_fi.ts \
                translations/garuda-boot-options_fr.ts \
                translations/garuda-boot-options_he_IL.ts \
                translations/garuda-boot-options_hi.ts \
                translations/garuda-boot-options_hr.ts \
                translations/garuda-boot-options_hu.ts \
                translations/garuda-boot-options_id.ts \
                translations/garuda-boot-options_is.ts \
                translations/garuda-boot-options_it.ts \
                translations/garuda-boot-options_ja.ts \
                translations/garuda-boot-options_kk.ts \
                translations/garuda-boot-options_ko.ts \
                translations/garuda-boot-options_lt.ts \
                translations/garuda-boot-options_mk.ts \
                translations/garuda-boot-options_mr.ts \
                translations/garuda-boot-options_nb.ts \
                translations/garuda-boot-options_nl.ts \
                translations/garuda-boot-options_pl.ts \
                translations/garuda-boot-options_pt.ts \
                translations/garuda-boot-options_pt_BR.ts \
                translations/garuda-boot-options_ro.ts \
                translations/garuda-boot-options_ru.ts \
                translations/garuda-boot-options_sk.ts \
                translations/garuda-boot-options_sl.ts \
                translations/garuda-boot-options_sq.ts \
                translations/garuda-boot-options_sr.ts \
                translations/garuda-boot-options_sv.ts \
                translations/garuda-boot-options_tr.ts \
                translations/garuda-boot-options_uk.ts \
                translations/garuda-boot-options_zh_CN.ts \
                translations/garuda-boot-options_zh_TW.ts

RESOURCES += \
    images.qrc

